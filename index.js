/* jshint esversion:6 */


function loadDiv(section){
    if (section === "home"){
        
        $('#rightbox').load('home.html');
    }else if (section === "lifecoach"){
        
        $('#rightbox').load('lifecoach.html');
    }else if (section === "therapy"){
        
        $('#rightbox').load('therapy.html');
    }else if (section === "contact"){
        
        $('#rightbox').load('contactus.html');
    }else if (section === "helpNow"){
        
        $('#rightbox').load('needhelp.html');
    }
}



document.getElementById("lifeCoachBtn").addEventListener("click", function(){
    
    loadDiv("lifecoach");
  });
document.getElementById("therapyBtn").addEventListener("click", function(){
    
    loadDiv("therapy");
  });

document.getElementById("contactBtn").addEventListener("click", function(){
    
    loadDiv("contact");
  });
document.getElementById("homeBtn").addEventListener("click", function(){
    
    loadDiv("home");
  });
document.getElementById("helpnow").addEventListener("click", function(){
    loadDiv("helpNow");
  });
